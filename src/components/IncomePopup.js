import React, { useState } from 'react';

const IncomePopup = ({ onSubmit }) => {
  const [income, setIncome] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(income);
  };

  return (
    <div className="income-popup">
      <form onSubmit={handleSubmit}>
        <label htmlFor="income">Enter your monthly income after tax:</label>
        <input
          type="number"
          id="income"
          value={income}
          onChange={(e) => setIncome(e.target.value)}
          required
        />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default IncomePopup;