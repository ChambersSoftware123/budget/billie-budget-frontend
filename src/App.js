import React, { useState, useEffect } from 'react';
import IncomePopup from './components/IncomePopup';
import Register from './Register';
import Login from './Login';

const App = () => {
  const [incomePopupVisible, setIncomePopupVisible] = useState(false);
  const [dailyBudget, setDailyBudget] = useState(null);
  const [remainingBudget, setRemainingBudget] = useState(null);
  const [spending, setSpending] = useState('');
  const [userId, setUserId] = useState(null);
  const [user, setUser] = useState(null);

  const YOUR_API_BASE_URL = 'http://localhost:8080';

  const handleRegister = async (username, password) => {
    const response = await fetch(`${YOUR_API_BASE_URL}/register?username=${username}&password=${password}`, {
      method: 'POST',
    });
    
    if (response.ok) {
      const data = await response.json();
      if (data.success) {
        setUser(data.user);
        setIncomePopupVisible(true);
      }
      return data;
    } else {
      // Handle the case where the response is not successful
      console.error('Registration failed:', response.status);
      return null;
    }
  };

  const handleLogin = async (username, password) => {
    const credentials = `${username}:${password}`;
    const encodedCredentials = btoa(credentials); // Base64 encode the credentials
  
    const response = await fetch(`${YOUR_API_BASE_URL}/login`, {
      method: 'POST',
      headers: {
        'Authorization': `Basic ${encodedCredentials}` // Include the Authorization header
      },
      credentials: 'include', // Include cookies with the request
    });
  
    // Check if the login was successful
    if (response.ok) {
      // Retrieve user data if needed
      const data = await response.json();
      setUser(data.user);
      setIncomePopupVisible(true);
      return data;
    } else {
      // Handle login failure
      console.error('Login failed');
      return null;
    }
};

  const handleIncomeSubmit = async (income) => {
    setIncomePopupVisible(false);
    const response = await fetch(`${YOUR_API_BASE_URL}/budgets`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ income }),
      redirect: 'follow',
      credentials: 'include',
    });
    const data = await response.json();
    setDailyBudget(data.dailyBudget);
    setUserId(data.userId);
  };

  useEffect(() => {
    if (userId) {
      const fetchRemainingBalance = async () => {
        const response = await fetch(`${YOUR_API_BASE_URL}/getRemainingBalance/${userId}`, {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' },
          redirect: 'follow',
          credentials: 'include',
        });
        const data = await response.json();
        setRemainingBudget(data.remainingBudget);
      };
      fetchRemainingBalance();
    }
  }, [userId]);

  const handleSpendingSubmit = async (e) => {
    e.preventDefault();
    const response = await fetch(`${YOUR_API_BASE_URL}/submitSpending`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ userId, spending }),
      credentials: 'include',
    });
    const data = await response.json();
    setRemainingBudget(data.remainingBudget);
    setSpending('');
  };

  return (
    <div className="App">
      {user ? (
        incomePopupVisible ? (
          <IncomePopup onSubmit={handleIncomeSubmit} />
        ) : (
          <div>
            <p>Daily Budget: {dailyBudget}</p>
            <p>Remaining Budget: {remainingBudget}</p>
            <form onSubmit={handleSpendingSubmit}>
              <label htmlFor="spending">Enter your spending:</label>
              <input
                type="number"
                id="spending"
                value={spending}
                onChange={(e) => setSpending(e.target.value)}
                required
              />
              <button type="submit">Submit</button>
            </form>
          </div>
        )
      ) : (
        <>
          <Register onRegister={handleRegister} />
          <Login onLogin={handleLogin} />
        </>
      )}
    </div>
  );
};

export default App;